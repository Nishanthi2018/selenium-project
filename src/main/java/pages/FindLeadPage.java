package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.*;
import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods{

	public FindLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "(//input[@name = 'firstName'])[3]") WebElement elefirstName;
	@FindBy(how = How.XPATH, using = "(//button[@class = 'x-btn-text'])[7]") WebElement eleFindLeads;
	@FindBy(how = How.XPATH, using = "(//a[@class = 'linktext'])[6]") WebElement elefirstnameclick;

	@Then("Enter the firstname as (.*)")
	public FindLeadPage enterFirstName(String data) {
		type(elefirstName, data);
		return this;
	}
	
	@Then("Click on Find Leads Button")
	public FindLeadPage clickFindLeadsButton() throws InterruptedException {
		click(eleFindLeads);
		Thread.sleep(3000);
		return this;
	}
	@Then("Verify Lead name found correctly")
	public ViewLeadPage clickFirstName() {
		click(elefirstnameclick);
		return new ViewLeadPage();
	}
	
	






}
