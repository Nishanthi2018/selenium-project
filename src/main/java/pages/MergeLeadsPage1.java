package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadsPage1 extends ProjectMethods{

	public MergeLeadsPage1() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "(//img[@alt = 'Lookup'])[1]") WebElement eleFromLeadIcon;
	@FindBy(how = How.XPATH, using = "(//img[@alt = 'Lookup'])[2]") WebElement eleToLeadIcon;
	
	public FindLeadWindowPage clickFromLead() {
		click(eleFromLeadIcon);
		switchToWindow(1);
		return new FindLeadWindowPage();	
	}
	
	public FindLeadWindowPage clickToLead() {
		click(eleToLeadIcon);
		switchToWindow(1);
		return new FindLeadWindowPage();
	}
	
}
