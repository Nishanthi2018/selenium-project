package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadWindowPage extends ProjectMethods{

	public FindLeadWindowPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@name = 'firstName']") WebElement elefirstName;
	@FindBy(how = How.XPATH, using = "//button[text() = 'Find Leads']") WebElement eleFindLeads;
	@FindBy(how = How.CLASS_NAME, using = "linktext") WebElement elefirstnameclick;

	public FindLeadWindowPage enterFirstName(String data) {
		type(elefirstName, data);
		return this;
	}
	
	public FindLeadWindowPage clickFindLeadsButton() throws InterruptedException {
		click(eleFindLeads);
		Thread.sleep(3000);
		return this;
	}
	public MergeLeadsPage1 clickFirstName() {
		String text = getText(elefirstName);
		click(elefirstnameclick);
		switchToWindow(0);
		return new MergeLeadsPage1();
	}
	
	
	
	






}
