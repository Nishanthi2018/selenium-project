package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "viewLead_companyName_sp") WebElement eleViewCompanyName;
	@FindBy(id = "viewLead_firstName_sp") WebElement eleViewFirstName;
	
	
	@Then("Verify Lead is Created")
	public ViewLeadPage verifyCompanyname(String data) {
		verifyPartialText(eleViewCompanyName, data);
		return this;
	}
	
	public ViewLeadPage verifyFirstName(String data) {
		verifyPartialText(eleViewFirstName, data);
		return this;
	}
		
}
