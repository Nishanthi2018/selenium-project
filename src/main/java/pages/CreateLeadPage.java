package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(id = "createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(id = "createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how = How.CLASS_NAME, using = "smallSubmit") WebElement eleCreateLead;

	@Then("Enter the Company Name as (.*)")
	public CreateLeadPage enterCompanyName(String data) {
		type(eleCompanyName, data);
		String text = getText(eleCompanyName);
		return this;
	}
	
	@Then("Enter the First Name as (.*)")
	public CreateLeadPage enterFirstName(String data) {
		type(eleFirstName, data);
		return this;
	}
	
	@Then("Enter the Last Name as (.*)")
	public CreateLeadPage enterLastName(String data) {
		type(eleLastName, data);
		return this;
	}
	
	@When("Click on Create Lead Button")
	public ViewLeadPage clickCreateLead() {
		click(eleCreateLead);
		return new ViewLeadPage();
	}
	
	

}
