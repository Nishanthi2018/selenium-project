package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadsPage2 extends ProjectMethods{

	public MergeLeadsPage2() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "(//img[@alt = 'Lookup'])[2]") WebElement eleToLeadIcon;
	@FindBy(how = How.LINK_TEXT, using = "Find Leads") WebElement eleFindLead;
	@FindBy(how = How.LINK_TEXT, using = "Merge Leads") WebElement eleMergeLead;

	/*public FindLeadWindowPage clickToLead() {
		click(eleToLeadIcon);
		return new FindLeadWindowPage();
		
	}
	public void clickMergeButton() {
		click(eleMergeButton);
		
	}*/
}
