package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{

	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT, using = "Create Lead") WebElement eleCreateLead;
	@FindBy(how = How.LINK_TEXT, using = "Find Leads") WebElement eleFindLead;
	@FindBy(how = How.LINK_TEXT, using = "Merge Leads") WebElement eleMergeLead;

	public MergeLeadsPage1 clickMergeLead() {
		click(eleMergeLead);
		return new MergeLeadsPage1();
	}
	
	@Then("Click on Create Lead")
	public CreateLeadPage clickCreateLead() {
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
	@Then("Click on Find Lead")
	public FindLeadPage clickFindLead() {
		click(eleFindLead);
		return new FindLeadPage();
		
	}

}
