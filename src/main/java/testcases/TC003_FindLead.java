package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_FindLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC003_Find Lead";
		testDescription = "Creating new lead";
		authors = "Nishanthi";
		category = "smoke";
		dataSheetName = "TC003_Find Leads";
		testNodes = "Leads";
	}
	@Test(dataProvider = "fetchData")
	public void findLeads(String username, String password, String firstname) throws InterruptedException {
		new LoginPage()
		.enterUserName(username)
		.enterPassword(password)
		.clickLogin()
		.clickLinkCRMSFA()
		.clickLeads()
		.clickFindLead()
		.enterFirstName(firstname)
		.clickFindLeadsButton()
		.clickFirstName()
		.verifyFirstName(firstname);
	}
}
