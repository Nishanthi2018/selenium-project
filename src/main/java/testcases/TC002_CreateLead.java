package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC002_Create Lead";
		testDescription = "Creating new lead";
		authors = "Nishanthi";
		category = "smoke";
		dataSheetName = "TC002_Create Lead";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void createLead(String username, String password, String companyname, String firstname, String lastname) {
		new LoginPage()
		.enterUserName(username)
		.enterPassword(password)
		.clickLogin()
		.clickLinkCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(companyname)
		.enterFirstName(firstname)
		.enterLastName(lastname)
		.clickCreateLead();
		//.verifyCompanyname(companyname);
		
	}
}
