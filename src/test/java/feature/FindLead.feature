Feature: Find Lead in Leaftaps

Scenario Outline: Find Lead
Given Enter the UserName as DemoSalesManager
And Enter the Password as crmsfa
When Click on the Login Button
And Click on CRMSFA link
And Click on Leads
And Click on Find Lead
And Enter the firstname as <firstname>
When Click on Find Leads Button
Then Verify Lead name found correctly

Examples:
|firstname|
|Nishanthi|