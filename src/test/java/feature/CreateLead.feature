Feature: Create Lead in Leaftaps

#Background:
#Given Open The Browser
#And Max the Browser
#And Set the TimeOut
#And Launch the URL

Scenario Outline: Create Lead
Given Enter the UserName as DemoSalesManager
And Enter the Password as crmsfa
When Click on the Login Button
Then Verify the Login
And Click on CRMSFA link
And Click on Leads
And Click on Create Lead
And Enter the Company Name as <companyName>
And Enter the First Name as <firstName>
And Enter the Last Name as <lastName>
When Click on Create Lead Button 
Then Verify Lead is Created


Examples:
|companyName|firstName|lastName|
|TestingCompany|TestFirst|TestLast|
|TestCompany|NishaFirst|NishaLast|
|ITCompany|Nishanthi|Rajendran|
