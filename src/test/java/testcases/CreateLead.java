/*package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	
	public ChromeDriver driver;
	
	@Given("Open The Browser")
	public void openTheBrowser() {
	    System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	    driver = new ChromeDriver();
	 }

	@Given("Max the Browser")
	public void maxTheBrowser() {
	    driver.manage().window().maximize();
	}

	@Given("Set the TimeOut")
	public void setTheTimeOut() {
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Given("Launch the URL")
	public void launchTheURL() {
	    driver.get("http://leaftaps.com/opentaps");
	}

	@Given("Enter the UserName as (.*)")
	public void enterUserName(String username) {
	    driver.findElementById("username").sendKeys(username);
	}

	@Given("Enter the Password as (.*)")
	public void enterPassword(String password) {
	    driver.findElementById("password").sendKeys(password);
	}

	@When("Click on the Login Button")
	public void clickOnLoginButton() {
	    driver.findElementByClassName("decorativeSubmit").click();
	}

	@Then("Verify the Login")
	public void verifyTheLogin() {
	    System.out.println("Login Successful");
	}

	@Then("Click on CRMSFA link")
	public void clickOnCRMSFALink() {
	    driver.findElementByLinkText("CRM/SFA").click();
	}

	@Then("Click on Leads")
	public void clickOnLeads() {
	    driver.findElementByLinkText("Leads").click();
	}

	@Then("Click on Create Lead")
	public void clickOnCreateLead() {
	    driver.findElementByLinkText("Create Lead").click();
	}

	@Then("Enter the Company Name as (.*)")
	public void enterCompanyName(String compname) {
	    driver.findElementById("createLeadForm_companyName").sendKeys(compname);
	}

	@Then("Enter the First Name as (.*)")
	public void enterFirstName(String firstname) {
	    driver.findElementById("createLeadForm_firstName").sendKeys(firstname);
	}

	@Then("Enter the Last Name as (.*)")
	public void enterLastName(String lastname) {
	    driver.findElementById("createLeadForm_lastName").sendKeys(lastname);
	}

	@When("Click on Create Lead Button")
	public void clickOnCreateLeadButton() {
	    driver.findElementByClassName("smallSubmit").click();
	}

	@Then("Verify Lead is Created")
	public void verifyLeadIsCreated() {
	    String text = driver.findElementById("viewLead_companyName_sp").getText();
	    if(text.contains("TestLeaf"))
	    	System.out.println("Lead has been created successfully");
	}
}
*/